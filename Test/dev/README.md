## Development test for Senior Magento Backend Developer at TwoDev.

## Test description
Magento module creation test
- create a module called "test" in package "twodev"
- create a sql script in the module to add the table "twodev_test" to DB with following column:
    - id (type int, auto increment)
    - value_1 (type varchar 255)
    - value_2 (type varchar 255)
    - value_3 (type varchar 255)
- create a controller with a method "index" accessible with URL domain_name/twodev/index
- create a model "Data" with function "insertData" and insert dummy data in the previously created table. 
- The function can be called with a method in the controller.
- create a function "getData" in the model to retrieve the lines of the table.
- render in the crontroller's index method the data retrieved with method getData in a HTML array.
- create a repository on github or bitbucket and commit the work.
Good luck!