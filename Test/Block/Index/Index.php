<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Block\Index;

class Index extends \Magento\Framework\View\Element\Template
{

    /**   
     * @var \Twodev\Test\Helper\Data 
     */
    protected  $dataHelper;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Framework\View\Element\Template\Context
     */  
    protected $context;  
    
    /**
     * @var \Twodev\Test\Model\ResourceModel\Data\CollectionFactory
     */
    protected $dataCollectionFactory;
    
    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Twodev\Test\Helper\Data $dataHelper
     * @param \Twodev\Test\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,                
        \Twodev\Test\Helper\Data $dataHelper, 
        \Twodev\Test\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory
    ) {
        $this->dataHelper = $dataHelper; 
        $this->dataCollectionFactory = $dataCollectionFactory;
        parent::__construct($context);
    }
    
    /**
     * Get the data collection
     * @return object
     */
    public function getDataCollection()
    {
        $collection = $this->dataCollectionFactory->create();

        return $collection;
    }      
    
    /**
     * Get the test page title
     * @return string
     */
    public function getTestPageTitle() 
    {
     return $this->dataHelper->getTestPageTitle();  
    }
        
    protected function _toHtml() 
    {  
        if (!$this->dataHelper->isModuleEnable())
        {
           return ''; 
        }
        
        return parent::_toHtml();
    }
}