<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Controller;

abstract class Index extends \Magento\Framework\App\Action\Action
{       
    /**
     * @var \Magento\Framework\App\Action\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
    
    /**
     * @var \Twodev\Test\Model\ResourceModel\Data\CollectionFactory
     */
    protected $dataCollectionFactory; 
    
    /**
     * @var \Twodev\Test\Model\DataFactory
     */
    protected $dataFactory;        
    
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Twodev\Test\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory
     * @param \Twodev\Test\Model\DataFactory $dataFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Twodev\Test\Model\ResourceModel\Data\CollectionFactory $dataCollectionFactory,
        \Twodev\Test\Model\DataFactory $dataFactory           
    ) {        
        $this->context = $context;
        $this->resultFactory = $context->getResultFactory();
        $this->dataCollectionFactory = $dataCollectionFactory; 
        $this->dataFactory = $dataFactory;                      
        parent::__construct($context);
    } 

   /**
     * @return \Twodev\Test\Model\Data
     */
    protected function _initData()
    {
        if ($id = $this->getRequest()->getParam('id')) 
        {
            $data = $this->dataCollectionFactory->create()
                    ->getFirstItem();
            
            if ($data->getId() > 0) 
            {
                $this->registry->register('current_data', $data);

                return $data;
            }
        }
    }

    protected function postData()
    {        
        $value1 = $this->getRequest()->getParam('value1');        
        $value2 = $this->getRequest()->getParam('value2');        
        $value3 = $this->getRequest()->getParam('value3');
        
        try {                    
                if ($value1 && $value2 && $value3)
                { 
                    $this->_insertData($value1, $value2, $value3);
                    $this->messageManager->addSuccessMessage(__('Your Data Has Been Successfully Saved'));                    
                }else{
                    $this->messageManager->addErrorMessage(__('You are missing at least one value, please check your data!'));
                }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } 
    } 
    
    protected function _insertData($value1, $value2, $value3)
    {
        $data = $this->dataFactory->create();
        $data->setValue1($value1)
                ->setValue2($value2)
                ->setValue3($value3);
        $data->save();

    }    
}
