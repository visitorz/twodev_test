<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Controller\Adminhtml\Data;

use Magento\Framework\Controller\ResultFactory;

class MassDelete extends \Magento\Backend\App\Action
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Twodev\Test\Model\ResourceModel\Data\CollectionFactory $collectionFactory
    ) {
        $this->context = $context;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if (!$this->getRequest()->getParams('namespace')) {
            return $resultRedirect->setPath('*/*/');
        }

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $data) {
            $data->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));


        return $resultRedirect->setPath('twodev/listing');
    }
 
    protected function _isAllowed()
    {
        return $this->context->getAuthorization()->isAllowed('Twodev_Test::test_data_listing');
    } 
}