<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Controller\Adminhtml\Listing;

use Magento\Framework;

class Index extends \Magento\Backend\App\Action
{   
    protected $resultPageFactory = false;
    
    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,          
        \Magento\Framework\View\Result\PageFactory $resultPageFactory      
    ) {
        parent::__construct($context);              
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('TwodevTest::test');
        $resultPage->addBreadcrumb(__('TwoDev Test - Data Listing'), __('TwoDev Test - Data Listing'));
        $resultPage->getConfig()->getTitle()->prepend(__('TwoDev Test - Data Listing'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Twodev_Test::test_data_listing');
    }
}