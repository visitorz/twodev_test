<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Postcontent extends \Twodev\Test\Controller\Index
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        $data = $this->_initData(); 
        
        if(!$data)
        { 
            $this->postData();
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        }
        return $resultRedirect->setPath('*/*/');        
    }
}
