<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{   
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;    
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    { 
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}
