<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Model\ResourceModel\Data;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{        
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    protected function _construct()
    {
        $this->_init(
            'Twodev\Test\Model\Data',
            'Twodev\Test\Model\ResourceModel\Data'
        );
    }
}

