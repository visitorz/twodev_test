<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper {

    const XML_CONFIG_PATH = 'twodev_test/twodev_test_config/';
    
    protected $scopeConfig;
    
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(\Magento\Framework\App\Helper\Context $context) 
    {
        parent::__construct($context);
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * Is the module enabled
     * @return boolean
     */
    public function isEnabled() 
    {
        return $this->scopeConfig->getValue(
                        self::XML_CONFIG_PATH . 'activation', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * Get test page title
     * @return string
     */
    public function getTestPageTitle() 
    {
        return $this->scopeConfig->getValue(
                        self::XML_CONFIG_PATH . 'test_page_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);                
    }
}