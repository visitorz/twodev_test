<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper 
{   
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;    
    
    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $context;
    
    /**
     * @var  \Twodev\Test\Helper\Config 
     */
    protected  $configHelper;

    /**
     * @var string 
     */
    protected $isModuleEnable;
    
    /**  
     * @var string
     */
    protected $testPageTitle;  
   
    /**
     * 
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Twodev\Test\Helper\Config $configHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Twodev\Test\Helper\Config $configHelper  
    ) 
    {
        $this->logger = $context->getLogger();       
        $this->configHelper = $configHelper;
        parent::__construct($context);    
    }
    
    /**
     * Is the module enabled
     * @return string
     */
    public function isModuleEnable() 
    {
        if($this->isModuleEnable)
        {
           return $this->isModuleEnable;
        }
        
        $isModuleEnable = $this->configHelper->isEnabled();
        $this->isModuleEnable = $isModuleEnable;
        
        return $isModuleEnable;
    }
    
    /**
     * Get the test page title
     * @return string
     */
    public function getTestPageTitle() 
    {
        if($this->testPageTitle)
        {
            return $this->testPageTitle;
        }
        
        $testPageTitle = $this->configHelper->getTestPageTitle();
        $this->testPageTitle = $testPageTitle;
        
        return $testPageTitle;
    }  
}
