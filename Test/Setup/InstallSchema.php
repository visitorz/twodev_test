<?php
/**
 * @category   Twodev
 * @package    Twodev/module-test
 * @author     issaberthet@gmail.com
 */

namespace Twodev\Test\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Twodev\Test\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        /**
         * Create table 'twodev_test'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('twodev_test')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Data ID'
        )->addColumn(
            'value_1',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Value 1'
        )->addColumn(
            'value_2',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Value 2'                
        )->addColumn(
            'value_3',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Value 3'                                
        )->setComment(
            'TwoDev Test main Table'
        );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}